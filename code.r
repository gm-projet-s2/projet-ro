library(e1071)
source("traitement.r")
data = read.table("MERA_O3_T.csv", sep=";", dec=".", header=TRUE, as.is=TRUE)

#on formate les données

data2 = traitementJours(data)
dataJour = data2[,1]
dataMax = as.numeric(data2[,2])
dataTemp = as.numeric(data2[,3])


dataJour = substring(dataJour,1,10)
dataJour = as.Date(dataJour,format = "%d/%m/%Y")

plot(dataJour, dataMax, type = "l",ylab="concentration moyenne horaire maximale journalière (µg/m^3)",xlab="date",main="Évolution du maximum journalier de la concentration moyenne horaire en ozone (station MERA 2014-2018)")

summa = summary(dataMax)
sd(dataMax)
kurtosis(dataMax)
skewness(dataMax)
hist(dataMax,xlab="concentration (µg/m^3)",ylab="fréquence",main="histogramme des maximums journaliers de la concentration moyenne horaire en ozone (station MERA 2014-2018)")
boxplot(dataMax,col="orange",ylab="concentration (µg/m^3)",main="boîte à moustache des maximums journaliers de la concentration moyenne horaire en ozone (station MERA 2014-2018)")
points(mean(dataMax))

l = length(data[,2])
dataHeures = list(c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c())
heure = 1
for (i in 1:l) 
{
  if (! is.na(data[i,2]) ) 
  {
    dataHeures[[heure]][length(dataHeures[[heure]]) + 1] = data[i,2]
  }
  heure = heure + 1
  if (heure > 24)
  {
    heure = 1
  }
}

boxplot(dataHeures[[1]], dataHeures[[2]], dataHeures[[3]], dataHeures[[4]], dataHeures[[5]], dataHeures[[6]], dataHeures[[7]], dataHeures[[8]], dataHeures[[9]], dataHeures[[10]], dataHeures[[11]], dataHeures[[12]], dataHeures[[13]], dataHeures[[14]], dataHeures[[15]], dataHeures[[16]], dataHeures[[17]], dataHeures[[18]], dataHeures[[19]], dataHeures[[20]], dataHeures[[21]], dataHeures[[22]], dataHeures[[23]], dataHeures[[24]],
        names = seq(1, 24, 1),
        col = "orange",xlab="heure de mesure",ylab="concentration moyenne horaire(µg/m^3)",main="concentrations moyennes horaires en ozone mesurées par la sation MERA (2014-2018)")
for (i in 1:24)
  points(mean(dataHeures[[i]]), x = i)


plot(x = dataMax, y = dataTemp,xlab="maximum journalier de concentration moyenne horaire(µg/m^3)",ylab="Température maximale journalière(°C)",main="Régression de la concentration maximale journalière en ozone avec la température(Station MERA 2014-2018)")
donnees <- data.frame(dataMax, dataTemp)
modele <- lm(dataTemp~dataMax, donnees)
print(modele)
summary(modele)
abline(modele, col="red")