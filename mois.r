source("traitement.r")
data = read.table("MERA_O3_T.csv", sep=";", dec=".", header=TRUE, as.is=TRUE)

#on formate les données
data2 = traitementJours(data)
dataJour = data2[,1]
dataMax = as.numeric(data2[,2])
dataJour = substring(dataJour,1,10)
dataJour = as.Date(dataJour,format = "%d/%m/%Y")
nbJoursValides = length(dataJour)

#on sépare les jours de la semaine
dataJourPOSIXLT = as.POSIXlt(dataJour)
dataMonth = dataJourPOSIXLT$mon + 1
dataMonth[which(dataMonth == 1)] = "janvier"
dataMonth[which(dataMonth == 2)] = "fevrier"
dataMonth[which(dataMonth == 3)] = "mars"
dataMonth[which(dataMonth == 4)] = "avril"
dataMonth[which(dataMonth == 5)] = "mai"
dataMonth[which(dataMonth == 6)] = "juin"
dataMonth[which(dataMonth == 7)] = "juillet"
dataMonth[which(dataMonth == 8)] = "aout"
dataMonth[which(dataMonth == 9)] = "septembre"
dataMonth[which(dataMonth == 10)] = "octobre"
dataMonth[which(dataMonth == 11)] = "novembre"
dataMonth[which(dataMonth == 12)] = "decembre"

dataMonth = factor(dataMonth, levels=c("janvier","fevrier","mars","avril","mai","juin","juillet","aout","septembre","octobre","novembre","decembre"))


boxplot(dataMax~dataMonth,col="orange",main="Maximum journalier de la concentration moyenne horaire en Ozone (station MERA 2014-2018)",xlab="mois",ylab="concentration moyenne horaire maximum journalière(µg/m^3)")

