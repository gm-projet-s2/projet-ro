source("traitement.r")
data = read.table("MERA_O3_T.csv", sep=";", dec=".", header=TRUE, as.is=TRUE)

#on formate les données
data2 = traitementJours(data)
dataJour = data2[,1]
dataMax = as.numeric(data2[,2])
dataJour = substring(dataJour,1,10)
dataJour = as.Date(dataJour,format = "%d/%m/%Y")
nbJoursValides = length(dataJour)

#on sépare les jours de la semaine
dataJourPOSIXLT = as.POSIXlt(dataJour)
datawday = dataJourPOSIXLT$wday

datawday = ifelse(datawday == 0,7,datawday)

lundi = c()
mardi = c()
mercredi = c()
jeudi = c()
vendredi = c()
samedi = c()
dimanche = c()
for (i in 1:nbJoursValides)
    {
    switch(datawday[i],
           {lundi[length(lundi)+1] = dataMax[i]},
           {mardi[length(mardi)+1] = dataMax[i]},
           {mercredi[length(mercredi)+1] = dataMax[i]},
           {jeudi[length(jeudi)+1] = dataMax[i]},
           {vendredi[length(vendredi)+1] = dataMax[i]},
           {samedi[length(samedi)+1] = dataMax[i]},
           {dimanche[length(dimanche)+1] = dataMax[i]})
    }

#on trace les boites à moustaches
boxplot(lundi, mardi, mercredi, jeudi, vendredi, samedi, dimanche,
        names = c("lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"),
        col = "orange",main="Maximums journaliers de la concentration moyenne horaire en Ozone (station MERA 2014-2018)",xlab="jour de la semaine",ylab="concentration moyenne horaire maximale journalière(µg/m^3)")
points(y=mean(lundi),x = 1)
points(y=mean(mardi),x = 2)
points(y=mean(mercredi),x = 3)
points(y=mean(jeudi),x = 4)
points(y=mean(vendredi),x = 5)
points(y=mean(samedi),x = 6)
points(y=mean(dimanche),x = 7)

#on calcule les statistiques de bases
summary(lundi)
sd(lundi)

summary(mardi)
sd(mardi)

summary(mercredi)
sd(mercredi)

summary(jeudi)
sd(jeudi)

summary(vendredi)
sd(vendredi)

summary(samedi)
sd(samedi)

summary(dimanche)
sd(dimanche)


