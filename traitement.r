"traitementJours" = function(data)
{
    l = length(data[,2])
    dataMax = c()
    dataJour = c()
    dataTempMaxConc = c()
    nbJours = l / 24
    nbJoursValides = 0
    i = 1
    j = 1
    for (i in 1:nbJours) 
    {
        nbHeuresValides = 0
        max = 0
        maxTemp = -50
        for (j in ((i - 1) * 24 + 1) : (i * 24)) 
        {
            if (! is.na(data[j,2]) ) 
            {
                nbHeuresValides = nbHeuresValides + 1
                if (data[j,2] > max) 
                {
                    max = data[j,2]
                }
            }
            if (! is.na(data[j,3]) ) 
            {
                if (data[j,3] > maxTemp) 
                {
                    maxTemp = data[j,3]
                }
            }
        }
        if (nbHeuresValides > 18)  # plus de 75 % des heures
        {
            nbJoursValides = nbJoursValides + 1
            dataMax[nbJoursValides] = max
            dataJour[nbJoursValides] = data[((i - 1) * 24 + 1),1] #pour garder quel jour correspond à quel max
            if (maxTemp == -50)
                maxTemp = NA
            dataTempMaxConc[nbJoursValides] = maxTemp
        }
    }
    return(matrix(c(dataJour,dataMax, dataTempMaxConc),nbJoursValides,3,byrow = FALSE, dimname = NULL ))
}

"traitementHeures" = function(data)
{
  l = length(data[,2])
  nbData = c(rep(0, 24))
  dataHeures = list(c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c(), c())
  heure = 1
  for (i in 1:l) 
  {
    if (! is.na(data[i,2]) ) 
    {
      dataHeures[[heure]][length(dataHeures[[heure]]) + 1] = data[i,2]
    }
    heure = heure + 1
    if (heure > 24)
    {
      heure = 1
    }
  }
  return(dataHeures)
}
